#include <stdio.h>

#include "../../include/local.h"
#include "bank.h"

int main( void )
{
    H_CASHCARD hCard1 = NULL;
    H_CASHCARD hCard2 = NULL;


    hCard1 = CreateBankAccount( "tamura yusuke", 0x1876 );
    hCard2 = CreateBankAccount( "kobuna hiroyo", 0x1835 );

    PrintBalance( hCard1 );
    PrintBalance( hCard2 );

    DepositMoney( hCard1, 1000000 );
    DepositMoney( hCard2, 500000 );

    PrintBalance( hCard1 );
    PrintBalance( hCard2 );

    WithdrawalMoney( hCard1, 0x1876, 10000 );
    WithdrawalMoney( hCard2, 0x1835,  10000 );

    PrintBalance( hCard1 );
    PrintBalance( hCard2 );

    DestroyBankAccount( hCard1, 0x1876 );
    DestroyBankAccount( hCard2, 0x1835 );

    return 0;
}


