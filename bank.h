#ifndef __BANK_H__
#define __BANK_H__


/*---------------------------------------------------*/
typedef void * H_CASHCARD;


/*---------------------------------------------------*/

PUBLIC H_CASHCARD   CreateBankAccount( const char *pName, unsigned short PinCode );
PUBLIC ERROR_T      PrintBalance( H_CASHCARD hCard );
PUBLIC ERROR_T      DepositMoney( H_CASHCARD hCard, unsigned long Money );
PUBLIC int          WithdrawalMoney( H_CASHCARD hCard, unsigned short PinCode, unsigned long RequestMoney );
PUBLIC ERROR_T      DestroyBankAccount( H_CASHCARD hCard, unsigned short PinCode );

#endif /* __BANK_H__ */

