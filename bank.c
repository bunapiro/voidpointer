#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../include/local.h"
#include "bank.h"


#define D_ACCOUNT_NAME_SIZE     (64)


typedef struct tag_BankAccount {

    char            Name[D_ACCOUNT_NAME_SIZE];  /* 登録者名 */
    unsigned short  PinCode;                    /* 暗唱番号 */
    unsigned long   Balance;                    /* 預金残高 */

} S_BANK_ACCOUNT_T;


PUBLIC H_CASHCARD CreateBankAccount( const char *pName, unsigned short PinCode )
{
    S_BANK_ACCOUNT_T *pAccount = NULL;


    if ( pName == NULL ) {

        return NULL;
    }


    pAccount = (S_BANK_ACCOUNT_T*)malloc( sizeof(S_BANK_ACCOUNT_T) );

    if ( pAccount == NULL ) {

        return NULL;
    }


    //strcpy_s( pAccount->Name, D_ACCOUNT_NAME_SIZE, pName );     /* strcpy_s( コピー先のアドレス, コピー先のバッファサイズ, コピー元のアドレス ) */
    strcpy( pAccount->Name, /* D_ACCOUNT_NAME_SIZE, */ pName );
    pAccount->PinCode = PinCode;
    pAccount->Balance = 0;

    return pAccount;
}


PUBLIC ERROR_T PrintBalance( H_CASHCARD hCard )
{
    S_BANK_ACCOUNT_T *pAccount = (S_BANK_ACCOUNT_T*)hCard;


    if ( pAccount == NULL ) {

        return FAILURE;
    }


    printf("-----------------------------\n");
    printf("Name:%s\n"     ,pAccount->Name );
    printf("Balance:%ld\n"  ,pAccount->Balance );

    return SUCCESS;
}


PUBLIC ERROR_T DepositMoney( H_CASHCARD hCard, unsigned long Money )
{
    S_BANK_ACCOUNT_T *pAccount = (S_BANK_ACCOUNT_T*)hCard;


    if ( pAccount == NULL ) {

        return FAILURE;
    }


    pAccount->Balance += Money;
}


PUBLIC int WithdrawalMoney( H_CASHCARD hCard, unsigned short PinCode, unsigned long RequestMoney )
{
    S_BANK_ACCOUNT_T *pAccount = (S_BANK_ACCOUNT_T*)hCard;


    if ( pAccount == NULL ) {

        return FAILURE;
    }

    if ( pAccount->PinCode != PinCode ) {

        return FAILURE;
    }

    if ( pAccount->Balance < RequestMoney ) {

        return FAILURE;
    }


    pAccount->Balance -= RequestMoney;


    return RequestMoney;
}


PUBLIC ERROR_T DestroyBankAccount( H_CASHCARD hCard, unsigned short PinCode )
{
    S_BANK_ACCOUNT_T *pAccount = (S_BANK_ACCOUNT_T*)hCard;


    if ( pAccount == NULL ) {

        return FAILURE;
    }

    if ( pAccount->PinCode != PinCode ) {

        return FAILURE;
    }


    FREE( pAccount );

    return SUCCESS;
}


